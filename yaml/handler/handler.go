package handler

import (
	"bytes"
	"io"

	"github.com/lucacasonato/frontmatter"

	abm "gitlab.com/antipy/antibuild/api/client"
	yaml "gopkg.in/yaml.v2"
)

// Version of the module
const Version = "1.0.0-pre17"

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("yaml", Version)

	module.DataParserRegister("yaml", parse)
	module.DataParserRegister("frontmatter", fm)

	module.CustomStart(stdin, stdout)
}

func parse(w abm.DPRequest, r abm.Response) {
	if w.Data == nil {
		r.AddWarning(abm.InvalidInput)
		return
	}

	var yamlData map[interface{}]interface{}

	err := yaml.Unmarshal(w.Data, &yamlData)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	var retData = make(map[interface{}]interface{}, len(yamlData))
	for k, v := range yamlData {
		retData[k] = v
	}

	r.AddData(retData)
}

func fm(w abm.DPRequest, r abm.Response) {
	if w.Data == nil {
		r.AddWarning(abm.InvalidInput)
		return
	}

	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	front, _, err := frontmatter.Parse(bytes.NewReader(w.Data), w.Variable)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	var yamlData = make(map[string]interface{})
	err = yaml.Unmarshal([]byte(front), &yamlData)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	var retData = make(map[interface{}]interface{}, len(yamlData))
	for k, v := range yamlData {
		retData[k] = v
	}

	r.AddData(retData)
}
