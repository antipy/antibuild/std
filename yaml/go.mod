module gitlab.com/antipy/antibuild/std/yaml

go 1.13

require (
	github.com/lucacasonato/frontmatter v1.0.0
	gitlab.com/antipy/antibuild/api v1.0.0-pre15
	gopkg.in/yaml.v2 v2.3.0
)
