module gitlab.com/antipy/antibuild/std/deno

go 1.14

require (
	github.com/bmatcuk/doublestar v1.3.0
	gitlab.com/antipy/antibuild/api v1.0.0-pre15
)
