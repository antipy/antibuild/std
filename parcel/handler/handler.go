// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package handler

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"github.com/bmatcuk/doublestar"
	abm "gitlab.com/antipy/antibuild/api/client"
	"gitlab.com/antipy/antibuild/api/errors"
)

var (
	currentInput   string
	currentReader  *bufio.Reader
	currentOutput  string
	currentProcess *exec.Cmd
	useBuild       bool
)

// Version of the module
const Version = "1.0.0-pre17"

//TODO: have a lock for parcel access
//TODO: handle addition of new html files in watch mode

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("parcel", Version)

	module.OutputPostProcessorRegister("watch", load)
	module.ConfigFunctionRegister(handleConfig)
	module.FinalizerFunctionRegister(killOldProcess)
	module.CustomStart(stdin, stdout)
}

func load(w abm.OPPRequest, r abm.Response) {
	if useBuild {
		r.AddDebug("Using build mode")
		files, _ := doublestar.Glob(w.InputFolder + "/**/*.html")

		//TODO: error checking!!
		wd, _ := os.Getwd()
		os.Chdir(w.InputFolder)
		wd2, _ := os.Getwd()
		r.AddDebug("Current working directory: " + wd2)
		cmd := exec.Command("parcel", append([]string{"build", "--no-optimize"}, append(files, "--dist-dir", w.OutputFolder)...)...)
		r.AddDebug("Running parcel: " + cmd.String())

		out, _ := cmd.StdoutPipe()
		reader := bufio.NewReader(out)
		cmd.Start()
		if err := readNextBuild(r, reader); err != "" {
			r.AddFatal("Failed to build:" + err)
		}
		os.Chdir(wd)
		return
	}
	r.AddDebug("Using watch mode")
	if w.InputFolder != currentInput || w.OutputFolder != currentOutput {
		err := startNewInstance(r, w.InputFolder, w.OutputFolder)
		if err != nil {
			r.AddFatal(fmt.Sprintf("error %e", err))
			return
		}
	}
	if err := readNextBuild(r, currentReader); err != "" {
		r.AddDebug("Retrying watch")
		//retry at least once
		err := startNewInstance(r, w.InputFolder, w.OutputFolder)
		if err != nil {
			r.AddFatal(fmt.Sprintf("error %e", err))
			return
		}
		if err := readNextBuild(r, currentReader); err != "" {
			//cant to anything now
			r.AddFatal("Failed to build: " + err)
			return
		}
	}
}

func startNewInstance(r abm.Response, input string, output string) error {
	killOldProcess()
	files, _ := doublestar.Glob(input + "/**/*.html")
	//TODO: error checking!!
	wd, _ := os.Getwd()
	os.Chdir(input)
	wd2, _ := os.Getwd()
	r.AddDebug("Current working directory: " + wd2)

	currentProcess = exec.Command("parcel", append(files, "--hmr-port", "8989", "--dist-dir", output)...)
	r.AddDebug("Running parcel: " + currentProcess.String())
	out, _ := currentProcess.StdoutPipe()
	reader := bufio.NewReader(out)
	err := currentProcess.Start()
	os.Chdir(wd)
	currentReader = reader
	currentInput = input
	currentOutput = output
	return err
}

func readNextBuild(r abm.Response, reader *bufio.Reader) string {
	var read string
	defer func() {
		var err error
		var line string
		//Read all lines untill eof.
		for err != nil {
			line, err = reader.ReadString('\n')
			read += line
		}
		r.AddInfo("Full standard out of the parcel command:\n" + read)
	}()
	for {
		line, err := reader.ReadString('\n')
		read += line
		if err != nil {
			return "Could not read data: " + err.Error()
		}
		if strings.HasPrefix(line, "✨ Built in") {
			return ""
		}
		if strings.HasPrefix(line, "🚨 Build failed.") {
			return "Build failed"
		}
	}
}

func killOldProcess() {
	if currentProcess != nil {
		if currentProcess.Process != nil {
			currentProcess.Process.Kill()
		}
		currentProcess = nil
	}
}

func handleConfig(input map[string]interface{}) *errors.Error {
	//panic(fmt.Sprintf("%v", input))
	if ab, ok := input["antibuild"]; ok {
		if v, ok := ab.(map[interface{}]interface{})["build_mode"]; ok {
			if s, ok := v.(string); ok && s == "build" {
				useBuild = true
				return nil
			}
		}
	}

	useBuild = false

	return nil
}
