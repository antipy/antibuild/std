package handler

import (
	abm "gitlab.com/antipy/antibuild/api/client"
)

func echo(w abm.DLRequest, r abm.Response) {
	r.AddData([]byte(w.Variable))
}
