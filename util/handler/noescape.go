package handler

import (
	"html/template"

	abm "gitlab.com/antipy/antibuild/api/client"
)

func noescapehtml(w abm.TFRequest, r abm.Response) {
	var args = make([]string, len(w.Data))
	var err bool

	for i, data := range w.Data {
		if args[i], err = data.(string); err == false {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := template.HTML(args[0])
	r.AddData(result)
	return
}

func noescapejs(w abm.TFRequest, r abm.Response) {
	var args = make([]string, len(w.Data))
	var err bool

	for i, data := range w.Data {
		if args[i], err = data.(string); err == false {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := template.JS(args[0])
	r.AddData(result)
	return
}
