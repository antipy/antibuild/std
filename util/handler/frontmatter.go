package handler

import (
	"bytes"

	"github.com/lucacasonato/frontmatter"

	abm "gitlab.com/antipy/antibuild/api/client"
)

func frontmatterBody(w abm.DPRequest, r abm.Response) {
	if w.Data == nil {
		r.AddWarning(abm.InvalidInput)
		return
	}

	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	_, rest, err := frontmatter.Parse(bytes.NewReader(w.Data), w.Variable)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	r.AddData(map[interface{}]interface{}{
		"body": rest,
	})
}
