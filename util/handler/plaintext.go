package handler

import (
	abm "gitlab.com/antipy/antibuild/api/client"
)

func plaintext(w abm.DPRequest, r abm.Response) {
	if w.Variable == "" {
		r.AddWarning(abm.InvalidInput)
		return
	}

	if w.Data == nil {
		r.AddWarning(abm.InvalidInput)
		return
	}

	var retData = make(map[interface{}]interface{})
	retData[w.Variable] = string(w.Data)

	r.AddData(retData)
}
