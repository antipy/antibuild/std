package handler

import (
	abm "gitlab.com/antipy/antibuild/api/client"
)

func prefix(w abm.DPPRequest, r abm.Response) {
	if w.Data == nil {
		r.AddFatal(abm.InvalidInput)
		return
	}

	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	var retData = make(map[interface{}]interface{})
	retData[w.Variable] = w.Data

	r.AddData(retData)
}
