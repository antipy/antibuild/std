package handler

import (
	"html/template"
	"io"

	abm "gitlab.com/antipy/antibuild/api/client"
)

// Version of the module
const Version = "1.0.0-pre17"

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("util", Version)

	module.DataLoaderRegister("echo", echo)

	module.DataParserRegister("plaintext", plaintext)
	module.DataParserRegister("frontmatterbody", frontmatterBody)

	module.DataPostProcessorRegister("prefix", prefix)

	module.TemplateFunctionRegister("noescapehtml", noescapehtml, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			"<h1>Test</h1>",
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: template.HTML("<h1>Test</h1>")},
		},
	})

	module.TemplateFunctionRegister("noescapejs", noescapejs, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			"console.log(\"Test\")",
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: template.JS("console.log(\"Test\")")},
		},
	})

	module.CustomStart(stdin, stdout)
}
