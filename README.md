# antibuild std

This is the repository for the antibuild standard library of modules. If you want to submit your module to the standard library please open a pull request.

# licence

All standard modules are licenced under the MIT licence. More details can be found in the LICENCE file.
