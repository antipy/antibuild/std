module gitlab.com/antipy/antibuild/std/json

go 1.13

require (
	github.com/lucacasonato/frontmatter v1.0.0
	gitlab.com/antipy/antibuild/api v1.0.0-pre15
)
