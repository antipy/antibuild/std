package handler

import (
	"bytes"
	"encoding/json"
	"io"

	"github.com/lucacasonato/frontmatter"

	abm "gitlab.com/antipy/antibuild/api/client"
)

// Version of the module
const Version = "1.0.0-pre17"

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("json", Version)

	module.DataParserRegister("json", parseJSON)
	module.DataParserRegister("frontmatter", fm)

	module.CustomStart(stdin, stdout)
}

func parseJSON(w abm.DPRequest, r abm.Response) {
	if w.Data == nil {
		r.AddFatal(abm.InvalidInput)
		return
	}

	var jsonData map[string]interface{}

	err := json.Unmarshal(w.Data, &jsonData)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	var retData = make(map[interface{}]interface{}, len(jsonData))
	for k, v := range jsonData {
		retData[k] = v
	}

	r.AddData(retData)
}

func fm(w abm.DPRequest, r abm.Response) {
	if w.Data == nil {
		r.AddWarning(abm.InvalidInput)
		return
	}

	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	if w.Data == nil {
		r.AddWarning(abm.InvalidInput)
		return
	}

	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	front, _, err := frontmatter.Parse(bytes.NewReader(w.Data), w.Variable)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	var jsonData map[string]interface{}
	err = json.Unmarshal([]byte(front), &jsonData)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	var retData = make(map[interface{}]interface{}, len(jsonData))
	for k, v := range jsonData {
		retData[k] = v
	}

	r.AddData(retData)
}
