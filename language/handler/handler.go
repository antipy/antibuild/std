package handler

import (
	"io"
	"path/filepath"

	abm "gitlab.com/antipy/antibuild/api/client"
	"gitlab.com/antipy/antibuild/api/errors"
	"gitlab.com/antipy/antibuild/api/site"
)

// Version of the module
const Version = "1.0.0-pre17"

var languages []string
var defaultLanguage string

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("language", Version)

	module.ConfigFunctionRegister(func(input map[string]interface{}) *errors.Error {
		var ok bool
		var languagesInterface []interface{}

		if languagesInterface, ok = input["languages"].([]interface{}); !ok {
			return &abm.ErrInvalidInput
		}

		for _, languageInterface := range languagesInterface {
			if language, ok := languageInterface.(string); ok {
				languages = append(languages, language)
			} else {
				return &abm.ErrInvalidInput
			}
		}

		if languages == nil {
			return &abm.ErrInvalidInput
		}

		if defaultLanguage, ok = input["default"].(string); !ok {
			return &abm.ErrInvalidInput
		}

		if defaultLanguage == "" {
			return nil
		}

		for _, language := range languages {
			if language == defaultLanguage {
				return nil
			}
		}

		return &abm.ErrInvalidInput
	})

	module.SitePostProcessorRegister("language", languageProcess)

	module.CustomStart(stdin, stdout)
}

func languageProcess(w abm.SPPRequest, r abm.Response) {
	var sites = w.Data

	if languages == nil {
		r.AddFatal(abm.NoConfig)
		return
	}

	data := make([]site.Site, len(sites)*len(languages))

	for pageNumber, page := range sites {
		for languageNumber, language := range languages {

			slugLanguage := language
			if language == defaultLanguage {
				slugLanguage = ""
			}

			var newData = make(map[interface{}]interface{})

			for i, v := range page.Data {
				//if this the language we asked for
				if i == language {
					if val, ok := v.(map[interface{}]interface{}); ok {
						for k, v := range val {
							newData[k] = v
						}
					} else {
						r.AddFatal("language data must be map[interface{}]interface{}")
						return
					}
				} else if k, ok := i.(string); ok && contains(languages, k) {
					continue
				} else {
					newData[i] = v
				}
			}

			data[pageNumber*len(languages)+languageNumber] = site.Site{
				Slug:     filepath.Join(slugLanguage, page.Slug),
				Template: page.Template,
				Data:     newData,
			}
		}
	}

	r.AddData(data)
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
