module gitlab.com/antipy/antibuild/std/file

go 1.16

require (
	github.com/fsnotify/fsnotify v1.4.9
	gitlab.com/antipy/antibuild/api v1.0.0-pre15
)
