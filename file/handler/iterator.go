package handler

import (
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strings"

	abm "gitlab.com/antipy/antibuild/api/client"
)

func folderIterator(w abm.ITRequest, r abm.Response) {
	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	files, err := ioutil.ReadDir(w.Variable)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	var list []string
	for _, file := range files {
		list = append(list, file.Name())
	}

	r.AddData(list)
}

func patternIterator(w abm.ITRequest, r abm.Response) {
	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	if strings.Count(w.Variable, "*") != 1 {
		r.AddFatal(abm.InvalidInput)
		return
	}

	matcher, err := regexp.Compile(strings.ReplaceAll(strings.ReplaceAll(w.Variable, ".", "\\."), "*", "(.*)") + "$")
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	path, err := filepath.Abs(w.Variable)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	files, err := filepath.Glob(path)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	retData := []string{}

	for _, file := range files {
		match := matcher.FindStringSubmatch(file)
		if len(match) == 2 && match[1] != "" {
			retData = append(retData, match[1])
		}
	}

	r.AddData(retData)
}
