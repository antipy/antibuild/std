package handler

import (
	"path/filepath"

	"github.com/fsnotify/fsnotify"
	abm "gitlab.com/antipy/antibuild/api/client"
)

var watcher *fsnotify.Watcher

func watchFile(w abm.DWRequest, r abm.Response) {
	if watcher != nil {
		watcher.Close()
	}
	if len(w.Variables) == 0 {
		r.AddFatal(abm.InvalidInput)
		return
	}
	// creates a new file watcher
	var err error
	watcher, err = fsnotify.NewWatcher()
	if err != nil {
		r.AddWarning("Unable to watch files!: " + err.Error())
	}

	absmap := make(map[string]string, len(w.Variables))
	for _, filePath := range w.Variables {
		abs, err := filepath.Abs(filePath)
		if err != nil {
			r.AddWarning(err.Error())
		}
		if err := watcher.Add(abs); err != nil {
			r.AddWarning(err.Error())
		}
		absmap[abs] = filePath
	}

	go func() {
		for {
			select {
			// watch for events
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}

				switch event.Op {
				case fsnotify.Write:
					file, err := filepath.Abs(event.Name)
					if err != nil {
						r.AddWarning(err.Error())
					}

					r.AddData(absmap[file])
				}
			// watch for errors
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				if err == nil {
					continue
				}
				r.AddWarning(err.Error())
			}
		}
	}()
}
