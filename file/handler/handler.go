package handler

import (
	"io"

	abm "gitlab.com/antipy/antibuild/api/client"
)

// Version of the module
const Version = "1.0.0-pre17"

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("file", Version)

	module.DataLoaderRegister("file", loadFile)
	module.DataWatcherRegister("file", watchFile)
	module.IteratorRegister("folder", folderIterator)
	module.IteratorRegister("pattern", patternIterator)

	module.CustomStart(stdin, stdout)
}
