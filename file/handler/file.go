package handler

import (
	"io/ioutil"

	abm "gitlab.com/antipy/antibuild/api/client"
)

func loadFile(w abm.DLRequest, r abm.Response) {
	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	file, err := ioutil.ReadFile(w.Variable)
	if err != nil {
		r.AddFatal(err.Error())
		return
	}

	r.AddData(file)
}
