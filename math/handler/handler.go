package handler

import (
	"io"

	abm "gitlab.com/antipy/antibuild/api/client"
)

// Version of the module
const Version = "1.0.0-pre17"

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("math", Version)

	module.TemplateFunctionRegister("add", add, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			1,
			2,
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: 3},
		},
	})

	module.TemplateFunctionRegister("subtract", subtract, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			3,
			1,
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: 2},
		},
	})
	module.TemplateFunctionRegister("multiply", multiply, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			3,
			2,
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: 6},
		},
	})

	module.TemplateFunctionRegister("divide", divide, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			6,
			2,
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: 3},
		},
	})

	module.TemplateFunctionRegister("modulo", modulo, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			5,
			2,
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: 1},
		},
	})

	module.TemplateFunctionRegister("power", power, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			3,
			3,
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: 65},
		},
	})

	module.CustomStart(stdin, stdout)
}

func add(w abm.TFRequest, r abm.Response) {
	var args = make([]int, len(w.Data))
	var ok bool

	for i, data := range w.Data {
		if args[i], ok = data.(int); !ok {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := args[0] + args[1]

	r.AddData(result)
	return
}

func subtract(w abm.TFRequest, r abm.Response) {
	var args = make([]int, len(w.Data))
	var ok bool

	for i, data := range w.Data {
		if args[i], ok = data.(int); !ok {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := args[0] - args[1]

	r.AddData(result)
	return
}

func multiply(w abm.TFRequest, r abm.Response) {
	var args = make([]int, len(w.Data))
	var ok bool

	for i, data := range w.Data {
		if args[i], ok = data.(int); !ok {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := args[0] * args[1]

	r.AddData(result)
	return
}

func divide(w abm.TFRequest, r abm.Response) {
	var args = make([]int, len(w.Data))
	var ok bool

	for i, data := range w.Data {
		if args[i], ok = data.(int); !ok {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := args[0] / args[1]

	r.AddData(result)
	return
}

func modulo(w abm.TFRequest, r abm.Response) {
	var args = make([]int, len(w.Data))
	var ok bool
	for i, data := range w.Data {
		if args[i], ok = data.(int); !ok {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := args[0] % args[1]

	r.AddData(result)
	return
}

func power(w abm.TFRequest, r abm.Response) {
	var args = make([]int, len(w.Data))
	var ok bool

	for i, data := range w.Data {
		if args[i], ok = data.(int); !ok {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := 1

	for index := 0; index < args[1]; index++ {
		result = result * args[0]
	}

	r.AddData(result)
	return
}
