build: build_darwin build_linux build_windows

build_darwin:
	export GOOS=darwin; \
		make build_amd64;

build_linux:
	export GOOS=linux; \
		make build_amd64;  \
		make build_386;

build_windows:
	export GOOS=windows; \
		make build_amd64; \
		make build_386;

build_amd64:
	export GOARCH=amd64; \
		make build_internal;

build_386:
	export GOARCH=386; \
		make build_internal;

build_internal:
	echo "Building ${PKG} for ${GOOS}/${GOARCH}";
	cd ${PKG}; go build -o ../dist/${GOOS}/${GOARCH}/abm_${PKG} main.go;

build_all:
	PKG=file make build
	PKG=json make build
	PKG=language make build
	PKG=markdown make build
	PKG=math make build
	PKG=util make build
	PKG=yaml make build
	PKG=deno make build
	PKG=parcel make build

test:
	cd ${PKG}; go test ./...

test_all:
	PKG=file make test
	PKG=json make test
	PKG=language make test
	PKG=markdown make test
	PKG=math make test
	PKG=util make test
	PKG=yaml make test
	PKG=deno make test
	PKG=parcel make test

update_api:
	cd file; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd json; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd language; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd markdown; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd math; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd util; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd yaml; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd deno; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true
	cd parcel; go get -d gitlab.com/antipy/antibuild/api@${VERSION} | true

go_tidy:
	cd file; go mod tidy | true
	cd json; go mod tidy | true
	cd language; go mod tidy | true
	cd markdown; go mod tidy | true
	cd math; go mod tidy | true
	cd util; go mod tidy | true
	cd yaml; go mod tidy | true
	cd deno; go mod tidy | true
	cd parcel; go mod tidy | true

tag:
	git tag -m 'branch: ${BRANCH}' -a file/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a json/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a language/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a markdown/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a math/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a util/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a yaml/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a deno/${VERSION}
	git tag -m 'branch: ${BRANCH}' -a parcel/${VERSION}

tag_push:
	git push origin file/${VERSION}
	git push origin json/${VERSION}
	git push origin language/${VERSION}
	git push origin markdown/${VERSION}
	git push origin math/${VERSION}
	git push origin util/${VERSION}
	git push origin yaml/${VERSION}
	git push origin deno/${VERSION}
	git push origin parcel/${VERSION}

un_tag:
	git tag -d file/${VERSION}
	git tag -d json/${VERSION}
	git tag -d language/${VERSION}
	git tag -d markdown/${VERSION}
	git tag -d math/${VERSION}
	git tag -d util/${VERSION}
	git tag -d yaml/${VERSION}
	git tag -d deno/${VERSION}
	git tag -d parcel/${VERSION}

update_version:
	sed -i 's/const Version = ".*"/const Version = "${VERSION}"/' */handler/handler.go