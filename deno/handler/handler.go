// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os/exec"
	"strings"

	abm "gitlab.com/antipy/antibuild/api/client"
)

// Version of the module
const Version = "1.0.0-pre17"

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("deno", Version)

	module.DataLoaderRegister("deno", loadFile)
	module.IteratorRegister("iterator", folderIterator)

	module.CustomStart(stdin, stdout)
}

func splitParameters(full string) (string, string) {
	// Check only if the ) is the last character, otherwise it might be part of the name.
	if strings.HasSuffix(full, ")") {
		for i, b := range reverse(full) {
			normalizedIndex := (len(full) - 1) - i
			var isInString bool
			switch b {
			//The only valid string indicator is `'`, `"` should not work.
			case '\'':
				isInString = !isInString
			case '(':
				if !isInString {
					println(normalizedIndex)
					return full[:normalizedIndex], full[normalizedIndex+1 : len(full)-1]
				}
			}
		}
		// If no opening ( is found, we must assume that the ) is part of the file name.
	}
	return full, ""
}

func loadFile(w abm.DLRequest, r abm.Response) {
	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	file, arg := splitParameters(w.Variable)

	stdout := &bytes.Buffer{}
	err := execDeno(file, arg, stdout)
	if err != nil {
		r.AddFatal(fmt.Sprintf("Failed to run %s using deno %v", w.Variable, err.Error()))
	}

	r.AddData(stdout.Bytes())
}

func folderIterator(w abm.ITRequest, r abm.Response) {
	if w.Variable == "" {
		r.AddFatal(abm.InvalidInput)
		return
	}

	file, arg := splitParameters(w.Variable)

	stdout := &bytes.Buffer{}
	err := execDeno(file, arg, stdout)
	if err != nil {
		r.AddFatal(fmt.Sprintf("Failed to run %s using deno %v", w.Variable, err.Error()))
	}

	var ret []string
	err = json.Unmarshal(stdout.Bytes(), &ret)
	if err != nil {
		r.AddFatal(fmt.Sprintf("Failed to parse result from iterator %s: %v", w.Variable, err.Error()))
	}

	r.AddData(ret)
}

const template = `import run from "./FILENAME";
	const data = await run(PARAMS);
	console.log(JSON.stringify(data));`

func execDeno(file, arg string, stdout *bytes.Buffer) error {
	in := strings.NewReplacer("FILENAME", file, "PARAMS", arg).Replace(template)

	cmd := exec.Command("deno", "eval", in, "--quiet", "--ts")
	cmd.Stdout = stdout
	stderr := &bytes.Buffer{}
	cmd.Stderr = stderr
	err := cmd.Start()
	if err != nil {
		return err
	}
	err = cmd.Wait()
	if err != nil {
		return err
	}
	return nil
}

func reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}
