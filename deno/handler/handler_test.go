package handler

import (
	"fmt"
	"testing"
)

type splitParametersTest struct {
	full string
	file string
	arg  string
}

var splitParametersTests = []splitParametersTest{
	{
		full: "./test.ts",
		file: "./test.ts",
		arg:  "",
	},
	{
		full: "./test.ts()",
		file: "./test.ts",
		arg:  "",
	},
	{
		full: "./test.ts('abc')",
		file: "./test.ts",
		arg:  "'abc'",
	},
	{
		full: "./test.ts(1 + 1)",
		file: "./test.ts",
		arg:  "1 + 1",
	},
	{
		full: "./test(.ts",
		file: "./test(.ts",
		arg:  "",
	},
	{
		full: "./test.ts('test123'",
		file: "./test.ts('test123'",
		arg:  "",
	},
	{
		full: "./test.ts'test123')",
		file: "./test.ts'test123')",
		arg:  "",
	},
}

func TestSplitParameters(t *testing.T) {
	for i, test := range splitParametersTests {
		t.Run(fmt.Sprintf("%v", i), func(t *testing.T) {
			file, arg := splitParameters(test.full)
			if file != test.file {
				t.Errorf("file does not match: is '%v' but expected '%v'", file, test.file)
			}
			if arg != test.arg {
				t.Errorf("arg does not match: is '%v' but expected '%v'", arg, test.arg)
			}
		})
	}
}
