package handler

import (
	"html/template"
	"io"

	abm "gitlab.com/antipy/antibuild/api/client"
	"gitlab.com/antipy/antibuild/api/errors"
	commonmark "gitlab.com/golang-commonmark/markdown"
)

// Version of the module
const Version = "1.0.0-pre17"

var commonmarkmd = commonmark.New(commonmark.Breaks(true), commonmark.Linkify(false))

// Handler is the starter for antibuild modules. It is seperated to allow for built-in modules
func Handler(stdin io.Reader, stdout io.Writer) {
	module := abm.Register("markdown", Version)

	module.ConfigFunctionRegister(newConfig)

	module.TemplateFunctionRegister("common", common, &abm.TFTest{
		Request: abm.TFRequest{Data: []interface{}{
			"# interface",
		}}, Response: &abm.TFResponse{
			Data: abm.TFResponseData{Data: "<h1>interface</h1>"},
		},
	})

	module.CustomStart(stdin, stdout)
}

func newConfig(cfg map[string]interface{}) *errors.Error {
	linkify := true
	langprefix := ""
	breaks := true
	if v, ok := cfg["linkify"]; ok {
		if l, ok := v.(bool); ok {
			linkify = l
		} else {
			err := errors.New("Linkify of bad type", errors.CodeWarning)
			return &err
		}
	}
	if v, ok := cfg["langprefix"]; ok {
		if l, ok := v.(string); ok {
			langprefix = l
		} else {
			err := errors.New("Langprefix of bad type", errors.CodeWarning)
			return &err
		}
	}
	if v, ok := cfg["extrabreaks"]; ok {
		if l, ok := v.(bool); ok {
			breaks = l
		} else {
			err := errors.New("Extrabreaks of bad type", errors.CodeWarning)
			return &err
		}
	}

	commonmarkmd = commonmark.New(func(m *commonmark.Markdown) {
		commonmark.Linkify(linkify)(m)
		commonmark.LangPrefix(langprefix)(m)
		commonmark.Breaks(breaks)(m)
	})
	return nil
}

func common(w abm.TFRequest, r abm.Response) {
	var args = make([]string, len(w.Data))
	var ok bool

	for i, data := range w.Data {
		if args[i], ok = data.(string); !ok {
			r.AddFatal(abm.InvalidInput)
			return
		}
	}

	result := template.HTML(commonmarkmd.RenderToString([]byte(args[0])))
	r.AddData(result)
}
