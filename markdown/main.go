// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"os"

	"gitlab.com/antipy/antibuild/std/markdown/handler"
)

func main() {
	handler.Handler(os.Stdin, os.Stdout)
}
