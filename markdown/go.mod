module gitlab.com/antipy/antibuild/std/markdown

go 1.13

replace gopkg.in/russross/blackfriday.v2 => github.com/russross/blackfriday/v2 v2.0.1+incompatible

require (
	gitlab.com/antipy/antibuild/api v1.0.0-pre15
	gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19
)
